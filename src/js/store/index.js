/* eslint-disable no-underscore-dangle */
import Vue from 'vue';
import Vuex from 'vuex';
import { nanoid } from 'nanoid';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    modals: [],
  },
  mutations: {
    ADD_MODAL(state, data) {
      state.modals.push(data);
    },
    DEL_MODAL(state, id) {
      if (!id) {
        console.log('cant del this modal, because needed UUID');
        return;
      }
      state.modals = state.modals.filter((el) => el.id !== id);
    },
  },
  actions: {
    addModal({ commit }, modal) {
      // show modal in App component
      const id = modal.id || nanoid();
      commit('ADD_MODAL', {
        id,
        ...modal,
      });
    },
  },
  modules: {
  },
});
