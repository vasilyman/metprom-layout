// JS
import './js/'

// SCSS
import './assets/scss/main.scss'

// Vue.js
window.Vue = require('vue')

// store
import { mapActions, mapState, mapMutations } from 'vuex';
import store from '@/store';

// plugins
import anime from 'animejs/lib/anime.es';

// Vue components (for use in html)
Vue.component('Chart', require('./js/components/Chart.vue').default)
Vue.component('mapcomponent', require('./js/components/Map.vue').default)

// Vue init
const app = new Vue({
  el: '#app',
  data: {
    showaside: false,
    cityMenu: {
      city: 'Москва',
      opened: false,
    },
    siteMenu: {
      active: ['Цены', 'Металлобазы'],
      vertical: false,
    },
    sectionPlot: {
      nameSelectOpen: false,
      sizeSelectOpen: false,
      classSelectOpen: false,
    },
    plotChartData: {},
    sectionCalc: {
      catSelectOpen: false,
      partSelectOpen: false,
    },
    scrollY: 0,
  },
  computed: {
    ...mapState(['modals']),
    plotChartOptions() {
      return {
        aspectRatio: 510 / 390,
        legend: {
          display: false,
        },
        scales: {
          xAxes: [{
            ticks: {
              fontSize: 10,
            },
          }],
          yAxes: [{
            ticks: {
              fontSize: 10,
            },
          }],
        },
      };
    },
    toTopShow() {
      return this.scrollY > 400;
    },
  },
  watch: {
    modals: {
      deep: true,
      handler(modals) {
        const { body } = document;
        if (body) body.style.overflow = modals.length > 0 ? 'hidden' : 'auto';
      }
    },
    'cityMenu.opened'(open) {
      this.toggleMenu(open, this.$refs['city-menu-content']);
    },
    'sectionPlot.nameSelectOpen'(open) {
      this.toggleMenu(open, this.$refs['plot-name-select-menu-content']);
    },
    'sectionPlot.sizeSelectOpen'(open) {
      this.toggleMenu(open, this.$refs['plot-size-select-menu-content']);
    },
    'sectionPlot.classSelectOpen'(open) {
      this.toggleMenu(open, this.$refs['plot-class-select-menu-content']);
    },
    'sectionCalc.catSelectOpen'(open) {
      this.toggleMenu(open, this.$refs['calc-cat-select-menu-content']);
    },
    'sectionCalc.partSelectOpen'(open) {
      this.toggleMenu(open, this.$refs['calc-part-select-menu-content']);
    },
    showaside: {
      immediate: true,
      handler(aside) {
        if (aside) {
          this.showAside();
        } else {
          this.hideAside();
        }
      },
    },
  },
  methods: {
    ...mapActions(['addModal']),
    ...mapMutations(['DEL_MODAL']),
    toggleMenu(open, targets) {
      if (targets) {
        anime({
          targets,
          maxHeight: open ? targets.scrollHeight : 0,
          easing: 'easeInOutQuad',
          duration: 200,
        });
      }
    },
    fillPlotChartData() {
      this.plotChartData = {
        labels: ['9.04.2020', '10.04.2020', '11.04.2020', '12.04.2020', ''],
        datasets: [
          {
            label: 'Data One',
            backgroundColor: 'transparent',
            borderColor: '#5893FB',
            data: [55000, 50000, 57000, 60000, 61000],
          },
        ],
      };
    },
    calcHeaderHeight() {
      const { header } = this.$refs;
      if (header) {
        const height = header.offsetHeight;
        document.documentElement.style.setProperty('--header-height', `${height}px`);
      }
    },
    resize() {
      this.calcHeaderHeight();
    },
    toTop() {
      const targets = document.documentElement;
      if (targets) {
        anime({
          targets,
          scrollTop: 0,
          duration: 400,
          easing: 'easeInOutQuart',
        });
      }
    },
    scroll() {
      this.scrollY = window.scrollY;
    },
    /** aside */
    showAside() {
      const targets = this.$refs.aside;
      const { body } = document;
      if (body) body.style.overflow = 'hidden';

      if (!targets) return;
      anime({
        targets,
        left: 0,
        duration: 400,
        easing: 'easeInOutQuart',
      });
    },
    hideAside() {
      const targets = this.$refs.aside;
      const { body } = document;
      if (body) body.style.overflow = 'auto';

      if (!targets) return;
      anime({
        targets,
        left: '-100%',
        duration: 400,
        display: 'none',
        easing: 'easeInOutQuart',
      });
    },
    onClickAside() {
      this.showaside = !this.showaside;
    },
  },
  mounted() {
    // add resize & scroll handler
    window.addEventListener('resize', this.resize);
    window.addEventListener('scroll', this.scroll);

    // initial set header height
    this.calcHeaderHeight();

    // hide city-menu
    this.toggleMenu(false, this.$refs['city-menu-content']);

    // section plot selects
    this.toggleMenu(false, this.$refs['plot-name-select-menu-content']);
    this.toggleMenu(false, this.$refs['plot-size-select-menu-content']);
    this.toggleMenu(false, this.$refs['plot-class-select-menu-content']);
    this.fillPlotChartData();

    // section part
    this.toggleMenu(false, this.$refs['calc-cat-select-menu-content']);
    this.toggleMenu(false, this.$refs['calc-part-select-menu-content']);
  },
  destroyed() {
    window.removeEventListener('resize', this.resize);
    window.removeEventListener('scroll', this.scroll);
  },
  store
})
